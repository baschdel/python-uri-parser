from uri import ParsedUri
from test import Test

parser = ParsedUri

Test(
	"https://someone@example.org:8080/test/foo.txt?something#somewhere",
	scheme = "https",
	hostinfo = "example.org:8080",
	host = "example.org",
	port = "8080",
	username = "someone",
	path = "/test/foo.txt",
	query = "something",
	index = "somewhere"
).test_parser(parser)

Test(
	"//example.org:blub/test",
	host = "example.org",
	port = "blub",
	hostinfo = "example.org:blub",
	path = "/test"
).test_parser(parser)

Test(
	"/foo:1.txt?#",
	path = "/foo:1.txt",
	query = "",
	index = "",
).test_parser(parser)

Test(
	"/foo:1.txt#?#",
	path = "/foo:1.txt",
	index = "?#",
).test_parser(parser)

Test(
	"https://www.example.org/?redirect_to=https://www.example.com",
	scheme = "https",
	hostinfo = "www.example.org",
	host = "www.example.org",
	path = "/",
	query = "redirect_to=https://www.example.com",
).test_parser(parser)

Test(
	"https://ocdoc.cil.li/component:modem",
	scheme = "https",
	hostinfo = "ocdoc.cil.li",
	host = "ocdoc.cil.li",
	path = "/component:modem",
).test_parser(parser)

Test(
	"./component:modem",
	path = "./component:modem",
).test_parser(parser)

Test(
	"//[::1]/test",
	path = "/test",
	hostinfo = "[::1]",
	host = "::1",
).test_parser(parser)

Test(
	"//jemand@[::1]:1965/test",
	path = "/test",
	hostinfo = "[::1]:1965",
	username = "jemand",
	host = "::1",
	port = "1965",
).test_parser(parser)

Test(
	"foo://example.com:8042/over/there?name=ferret#nose",
	scheme = "foo",
	hostinfo = "example.com:8042",
	host = "example.com",
	port = "8042",
	path = "/over/there",
	query = "name=ferret",
	index = "nose",
).test_parser(parser)

Test(
	"urn:example:animal:ferret:nose",
	scheme = "urn",
	path = "example:animal:ferret:nose",
).test_parser(parser)

Test(
	"irc://chat.freenode.net:6697/#example",
	parse_index = False,
	scheme = "irc",
	hostinfo = "chat.freenode.net:6697",
	host = "chat.freenode.net",
	port = "6697",
	path = "/#example"
).test_parser(parser)

Test(
	"#example",
	parse_index = False,
	path = "#example"
).test_parser(parser)

Test(
	"mailto:jemand@example.org",
	scheme = "mailto",
	path = "jemand@example.org"
).test_parser(parser)

Test(
	"tel:+1-816-555-1212",
	scheme = "tel",
	path = "+1-816-555-1212"
).test_parser(parser)

Test(
	"https://irgent:jemand@example.org:6:7/rss",
	scheme = "https",
	hostinfo = "example.org:6:7",
	host = "example.org",
	port = "6:7",
	path = "/rss",
	username = "irgent:jemand"
).test_parser(parser)

Test(
	"ftp://hostname/path@with/at",
	scheme = "ftp",
	hostinfo = "hostname",
	host = "hostname",
	path = "/path@with/at",
).test_parser(parser)

Test(
	"ftp://someone@hostname",
	scheme = "ftp",
	username = "someone",
	hostinfo = "hostname",
	host = "hostname",
).test_parser(parser)

Test(
	"foo://#bar",
	scheme = "foo",
	index = "bar",
	hostinfo = "",
	host = "",
).test_parser(parser)

Test(
	"file:///home",
	scheme = "file",
	path = "/home",
	hostinfo = "",
	host = "",
).test_parser(parser)
