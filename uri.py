class ParsedUri:
	def __init__(self, uri = None, parse_query = True, parse_index = True):
		self.scheme = None
		self.hostinfo = None
		self.index = None
		self.query = None
		self.path = None
		self.username = None
		# from hostinfo
		self.host = None
		self.port = None
		# settings
		self.parse_query = parse_query
		self.parse_index = parse_index
		# das eigentliche parsen
		if type(uri) is str:
			self.parse_uri(uri)
		if type(self.hostinfo) is str:
			self.parse_hostinfo(self.hostinfo)
	
	def parse_uri(self, uri):
		# Besondere Zeichen finden
		index_of_hash = uri.find("#") # end of uri-query
		# uri-index extrahieren
		# wenn wir einen uri-index haben und den auch extra haben wollen
		if (index_of_hash >= 0 and self.parse_index):
			self.index = uri[index_of_hash+1:]
		else:
			index_of_hash = len(uri) # end of query
		index_of_questionmark = uri.find("?") # end of uri-path +1
		# uri-query extrahieren
		# wenn wir einen uri-query haben und den auch extra haben wollen und das erste fragezeichen vor dem ersten hash ist
		if (index_of_questionmark >= 0 and index_of_questionmark < index_of_hash and self.parse_query):
			self.query = uri[index_of_questionmark+1:index_of_hash]
		else:
			index_of_questionmark = index_of_hash # end of path
		# Nochmal andre besondere zeichen finden
		# index_of_questionmark ist jetzt das ende von dem für
		# den ab hier relevanten Teil der uri
		index_of_colon = uri.find(":") # start of uri-authority -1
		index_of_slash = uri.find("/")
		# wenn wir einen doppelpunkt haben und
		# der nicht in dem Teil liegt, den wir schon bearbeitet haben und
		# vor dem ersten slash ist
		# dann haben wir ein uri-scheme
		if (index_of_colon >= 0 and index_of_colon < index_of_questionmark and (index_of_colon < index_of_slash or index_of_slash == -1)):
			self.scheme = uri[:index_of_colon]
		else:
			index_of_colon = -1
		# vom start of authority bis zum end of path rausschneiden
		part_uri = uri[index_of_colon+1:index_of_questionmark]
		# wenn unsere teil-uri mit nem doppelslash beginnt wissen wir, dass da ne authority drin is
		if (part_uri.startswith("//")):
			# wenn dieser teil uri ansonsten leer ist können wir uns den rest sparen
			#if (len(part_uri) == 2): #einfacher zu erklären, wenn der teil weg ist
			#	self.hostinfo = ""
			#else:
			#nach path anfang suchen
			index_of_slash = part_uri.find("/",2) #end of uri-authority
			# uri path rausschneiden
			if (index_of_slash >= 0):
				# uri-path besteht aus dem slash und allem was danach kommt
				self.path = part_uri[index_of_slash:]
			else:
				index_of_slash = len(part_uri) #end of uri-authority
			# haben wir ein "@", wenn ja wo?
			index_of_at = part_uri.find("@") # start of uri-authority
			# nutzername kommt vor dem slash
			if (index_of_at >= 0 and index_of_at < index_of_slash):
				self.username = part_uri[2:index_of_at]
			else:
				index_of_at = 1
			# hostinfo ist das was übrig bleibt, wenn man nutzername und pfad wegeschnitten hat
			self.hostinfo = part_uri[index_of_at+1:index_of_slash]
		else:
			# ohne authority gehört das ganze ding zum uri-pfad
			self.path = part_uri
	
	def parse_hostinfo(self, hostinfo):
		# wenn der hostname mit ner eckigen klammer beginnt
		# beginnt der port nach einer schliessenden eckigen klammer
		hostname_start = 0 # eingeschlossen
		hostname_end = None # ausgeschlossen
		# wenn hostinfo mit "[" beginnt
		if (hostinfo.startswith("[")):
			hostname_start = 1
			hostname_end = hostinfo.find("]")
		# beginne am ende vom hostnamen (oder am Anfang, wenn nicht definiert) mit der Suche nach dem doppelpunkt
		# hostname_end kann hier -1 sein, juckt uns aber weing weil die find funktion
		# damit klar kommt und einfach am anfang mit der suche beginnt
		# wenn man komische sachen reinschmeisst können halt auch komische sachen rauskommen
		index_of_colon = hostinfo.find(":",hostname_end or 0)
		# wenn's einen doppelpunkt gibt gehört alles nach dem doppelpunkt zum port
		if (index_of_colon >= 0):
			self.port = hostinfo[index_of_colon+1:]
			# wenn das ende vom hostnamen nicht definiert ist endets vor'm doppelpunkt
			hostname_end = hostname_end or index_of_colon
		# host ausschneiden
		self.host = hostinfo[hostname_start:hostname_end]
			
