class Test:
	def __init__(self,
	             uri,
	             parse_query = True,
	             parse_index = True,
	             scheme = None,
	             hostinfo = None,
	             index = None,
	             query = None,
	             path = None,
	             username = None,
	             host = None,
	             port = None):
		# input
		self.uri = uri
		self.parse_query = parse_query
		self.parse_index = parse_index
		# expected results
		self.scheme = scheme
		self.hostinfo = hostinfo
		self.index = index
		self.query = query
		self.path = path
		self.username = username
		# from hostinfo
		self.host = host
		self.port = port
	
	def test_assert(self,name,value,expected):
		if value == expected:
			print("\t       "+name+"\t "+repr(value))
		else:
			print("\t[FAIL] "+name+"\t "+repr(value)+" should be "+repr(expected))
	
	def test_parser(self,parser):
		print("Testing: "+self.uri)
		parsed_uri = parser(self.uri, self.parse_query, self.parse_index)
		if (not parsed_uri.parse_query):
			print("\tNot parsing query.")
		if (not parsed_uri.parse_index):
			print("\tNot parsing index.")
		self.test_assert("scheme", parsed_uri.scheme, self.scheme)
		self.test_assert("hostinfo", parsed_uri.hostinfo, self.hostinfo)
		self.test_assert("username", parsed_uri.username, self.username)
		self.test_assert("host", parsed_uri.host, self.host)
		self.test_assert("port", parsed_uri.port, self.port)
		self.test_assert("path", parsed_uri.path, self.path)
		self.test_assert("query", parsed_uri.query, self.query)
		self.test_assert("index", parsed_uri.index, self.index)
